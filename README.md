Simple skeleton macro to analyze BACCARAT data in the flat table ROOT output.

Use by: root -b -q Baccarat_Analysis.C
